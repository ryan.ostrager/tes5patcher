#[macro_use]
extern crate nom;

use std::io::prelude::*;
use std::fs::File;
use nom::*;
use std::str;
mod records;
fn main() {
    println!("Hello, world!");
    parse_from_file("..\\data\\testplugin.esp");
}


fn parse_from_file(filename: &str) {
	let mut f = File::open(filename).unwrap();
    let mut buffer = vec!();

    f.read_to_end(&mut buffer).unwrap();
    parse_from_slice(&buffer);
}

fn parse_from_slice(slice: &[u8]) {
	let mut remaining = slice;
	while remaining.len()>0 {
    let grup = switch!(remaining, take!(4),
      b"TES4" => call!(parse_record)|
      b"GRUP" => call!(parse_grup)|
      b"GMST" => call!(parse_record)|
      b"KYWD" => call!(parse_record)|
      b"LCRT" => call!(parse_record)|
      b"AACT" => call!(parse_record)|
      b"TXST" => call!(parse_record)|
      b"GLOB" => call!(parse_record)|
      b"CLAS" => call!(parse_record)|
      b"FACT" => call!(parse_record)|
      b"HDPT" => call!(parse_record)|
      b"EYES" => call!(parse_record)|
      b"RACE" => call!(parse_record)|
      b"SOUN" => call!(parse_record)|
      b"ASPC" => call!(parse_record)|
      b"MGEF" => call!(parse_record)|
      b"LTEX" => call!(parse_record)|
      b"ENCH" => call!(parse_record)|
      b"SPEL" => call!(parse_record)|
      b"SCRL" => call!(parse_record)|
      b"ACTI" => call!(parse_record)|
      b"TACT" => call!(parse_record)|
      b"ARMO" => call!(parse_record)|
      b"BOOK" => call!(parse_record)|
      b"CONT" => call!(parse_record)|
      b"DOOR" => call!(parse_record)|
      b"INGR" => call!(parse_record)|
      b"LIGH" => call!(parse_record)|
      b"MISC" => call!(parse_record)|
      b"APPA" => call!(parse_record)|
      b"STAT" => call!(parse_record)|
      b"MSTT" => call!(parse_record)|
      b"GRAS" => call!(parse_record)|
      b"TREE" => call!(parse_record)|
      b"FLOR" => call!(parse_record)|
      b"FURN" => call!(parse_record)|
      b"WEAP" => call!(parse_record)|
      b"AMMO" => call!(parse_record)|
      b"NPC_" => call!(parse_record)|
      b"LVLN" => call!(parse_record)|
      b"KEYM" => call!(parse_record)|
      b"ALCH" => call!(parse_record)|
      b"IDLM" => call!(parse_record)|
      b"COBJ" => call!(parse_record)|
      b"PROJ" => call!(parse_record)|
      b"HAZD" => call!(parse_record)|
      b"SLGM" => call!(parse_record)|
      b"LVLI" => call!(parse_record)|
      b"WTHR" => call!(parse_record)|
      b"CLMT" => call!(parse_record)|
      b"SPGD" => call!(parse_record)|
      b"RFCT" => call!(parse_record)|
      b"REGN" => call!(parse_record)|
      b"NAVI" => call!(parse_record)|
      b"CELL" => call!(parse_record)|
      b"REFR" => call!(parse_record)|
      b"ACHR" => call!(parse_record)|
      b"PGRE" => call!(parse_record)|
      b"NAVM" => call!(parse_record)|
      b"PHZD" => call!(parse_record)|
      b"WRLD" => call!(parse_record)|
      b"LAND" => call!(parse_record)|
      b"DIAL" => call!(parse_record)|
      b"INFO" => call!(parse_record)|
      b"QUST" => call!(parse_record)|
      b"IDLE" => call!(parse_record)|
      b"PACK" => call!(parse_record)|
      b"CSTY" => call!(parse_record)|
      b"LSCR" => call!(parse_record)|
      b"LVSP" => call!(parse_record)|
      b"ANIO" => call!(parse_record)|
      b"WATR" => call!(parse_record)|
      b"EFSH" => call!(parse_record)|
      b"EXPL" => call!(parse_record)|
      b"DEBR" => call!(parse_record)|
      b"IMGS" => call!(parse_record)|
      b"IMAD" => call!(parse_record)|
      b"FLST" => call!(parse_record)|
      b"PERK" => call!(parse_record)|
      b"BPTD" => call!(parse_record)|
      b"ADDN" => call!(parse_record)|
      b"AVIF" => call!(parse_record)|
      b"CAMS" => call!(parse_record)|
      b"CPTH" => call!(parse_record)|
      b"VTYP" => call!(parse_record)|
      b"MATT" => call!(parse_record)|
      b"IPCT" => call!(parse_record)|
      b"IPDS" => call!(parse_record)|
      b"ARMA" => call!(parse_record)|
      b"ECZN" => call!(parse_record)|
      b"LCTN" => call!(parse_record)|
      b"MESG" => call!(parse_record)|
      b"DOBJ" => call!(parse_record)|
      b"LGTM" => call!(parse_record)|
      b"MUSC" => call!(parse_record)|
      b"FSTP" => call!(parse_record)|
      b"FSTS" => call!(parse_record)|
      b"SMBN" => call!(parse_record)|
      b"SMQN" => call!(parse_record)|
      b"SMEN" => call!(parse_record)|
      b"DLBR" => call!(parse_record)|
      b"MUST" => call!(parse_record)|
      b"DLVW" => call!(parse_record)|
      b"WOOP" => call!(parse_record)|
      b"SHOU" => call!(parse_record)|
      b"EQUP" => call!(parse_record)|
      b"RELA" => call!(parse_record)|
      b"SCEN" => call!(parse_record)|
      b"ASTP" => call!(parse_record)|
      b"OTFT" => call!(parse_record)|
      b"ARTO" => call!(parse_record)|
      b"MATO" => call!(parse_record)|
      b"MOVT" => call!(parse_record)|
      b"SNDR" => call!(parse_record)|
      b"DUAL" => call!(parse_record)|
      b"SNCT" => call!(parse_record)|
      b"SOPM" => call!(parse_record)|
      b"COLL" => call!(parse_record)|
      b"CLFM" => call!(parse_record)|
      b"REVB" => call!(parse_record)
    ).unwrap();
    remaining = grup.0;
	}	
}

fn parse_grup(slice: &[u8]) -> IResult<&[u8], &[u8]>{
    let result = tuple!(slice,
        le_u32,
        le_u32,
        le_u32,
        le_u16,
        le_u16,
        le_u16,
        le_u16
      ).unwrap();
    let (size, label, group_type, stamp, unk_1, version, unk_2) = result.1;
    take!(result.0,size)
}

fn parse_record(slice: &[u8]) -> IResult<&[u8], &[u8]>{
    let result = tuple!(slice,
        le_u32,
        le_u32,
        le_u32,
        le_u16,
        le_u16,
        le_u16,
        le_u16
      ).unwrap();
    let (size, label, group_type, stamp, unk_1, version, unk_2) = result.1;
    take!(result.0,size)
}
#[test]
fn test() {
	parse_from_file("data\\testplugin.esp");
}