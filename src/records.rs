use nom::*;

pub struct TES4<'a> {
	version: f32,
	num_records: i32,
	next_object_id: u32,
	author: &'a str,
	description: &'a str,
	master: &'a str,
	size: u64,
	overrides: Vec<u32>,
	unk_1: u32,
	unk_2: u32
}

pub fn parse_TES4(slice: &[u8]) -> TES4 {
	TES4 {
		version: 0f32,
		num_records: 0,
		next_object_id: 0,
		author: "test",
		description: "test2",
		master: "test3",
		size: 0,
		overrides: vec!(),
		unk_1: 0,
		unk_2: 0
	}
}